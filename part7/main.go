package main

import (
	"fmt"
	"os"
)

type stringer interface {
	string() string
}

type tester interface {
	stringer
	test()
}

func (*data) test() {}

type Integer int

func (a Integer) Less(b Integer) bool {
	return a < b
}

func (a Integer) Add(b Integer) {
	a += b
}

type LessAdder interface {
	Less(b Integer) bool
	Add(b Integer)
}
type data int

func (d data) String() string {
	return fmt.Sprintf("data:%d", d)
}
func main() {
	var d data = 15
	var x interface{} = d
	if n, ok := x.(fmt.Stringer); ok { //判断x是否实现了fmt.Stringer接口(类型的方法集包含String方法就实现了fmt.Stringer接口
		fmt.Println(n)
	}
	if d2, ok := x.(data); ok { //判断x的类型是否为data类型
		fmt.Println(d2)
	}
	//e := x.(error) //panic: interface conversion: main.data is not error: missing method Error
	//fmt.Println(e)

	//正确方式
	if e3, ok := x.(error); ok {
		fmt.Println(e3)
	}
	os.Exit(0)
	a := 1
	switch a {
	case 1:
	case 2:
		println(2)
	}
	os.Exit(a)
	var v1 interface{} = 5
	switch v1.(type) {
	case int:
		fmt.Println("type is int")
	case string:
		fmt.Println("type is string")
	}
	os.Exit(0)
	/*var a Integer = 1
	var b LessAdder = a
	var b LessAdder = &a
	//var m map[string]string
	var a = [3]int{1, 2, 3}
	var b = a
	b[1]++
	fmt.Println(a, b)
	os.Exit(0)
	var t interface { //定义匿名接口变量
		string() string
	} = data{}
	n := node{
		data: t,
	}
	println(n.data.string())*/

}
