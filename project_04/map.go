package main

import "fmt"

func main() {
	m1 := map[string]int{}
	m1["a"] = 1
	m1["b"] = 2
	m1["c"] = 3
	m1["d"] = 4
	//delete(m1, "b")
	for k, v := range m1 {
		fmt.Printf("key=%s,value=%d\n", k, v)
	}
	//fmt.Println(m1)
}
