package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type Movie struct {
	Title  string
	Year   int  `json:"released"`
	Color  bool `json:"color,omitempty"`
	Actors []string
}

func main() {
	var movie = []Movie{
		{Title: "三枪", Year: 2008, Color: false,
			Actors: []string{"张艺谋", "张二谋"}},
		{Title: "1942", Year: 2011, Color: true,
			Actors: []string{"冯小刚", "冯大纲"}},
		{Title: "让子弹飞", Year: 2010, Color: true,
			Actors: []string{"姜文"}},
	}
	data, err := json.Marshal(movie)
	if err != nil {
		log.Fatalf("JSON marshaling failed: %s", err)
	}
	fmt.Printf("%s\n", data)
	//log.Fatalf("JSON marshaling failed: %s", "err")
	data2, _ := json.MarshalIndent(movie, "", "    ")
	//data3, _ := json.MarshalIndent(movie, "$$$", "---")
	fmt.Printf("%s\n", data2)
	//fmt.Printf("%s\

}
