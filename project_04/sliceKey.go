package main

import "fmt"

var m = make(map[string]int)

func main() {
	s1 := []string{"aaa", "bbb", "ccc"}
	s2 := []string{"ddd", "eee", "fff"}
	Add(s1)
	Add(s2)
	fmt.Println(m)
	fmt.Println(Count(s1))
	fmt.Println(Count(s2))

}

//将切片转为字符串
func k(list []string) string {
	return fmt.Sprintf("%q", list)
}

//将slice作为key给map
func Add(list []string) {
	m[k(list)]++
}

//根据slice找出对应的数字
func Count(list []string) int {
	return m[k(list)]
}
