package main

import "fmt"

func main() {
	b := equal(map[string]int{"A": 0}, map[string]int{"B": 42})
	fmt.Println(b)

}

func equal(x, y map[string]int) bool {
	if len(x) != len(y) {
		return false
	}
	for k, v1 := range x {
		if v2, ok := y[k]; !ok || v2 != v1 {
			return false
		}
	}
	return true
}
