package main

import "fmt"

func main() {
	fmt.Println(nonempty2([]string{"", "a", "b"}))
}

func nonempty2(strings []string) []string {
	out := strings[:0]
	//fmt.Println(out)
	for _, s := range strings {
		if s != "" {
			out = append(out, s)
		}
		//fmt.Println(out)
	}
	return out
}
