package main

import "fmt"

type Currency int

const (
	USD Currency = iota
	EUR
	GBP
	RMB
)

func main() {
	//q := [...]int{1, 2, 3}
	//fmt.Printf("%T\n", q)
	symbol := [...]string{USD: "$", RMB: "￥"}
	fmt.Println(RMB, symbol[RMB])

	r := [...]int{10: -1}
	fmt.Println(r)
}
