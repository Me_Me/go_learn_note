package main

import "fmt"

type Point struct {
	X, Y int
}
type Circle struct {
	Center Point
	Radius int
}

type Wheel struct {
	Circle Circle
	Spokes int
}

func main() {
	var w Wheel
	w = Wheel{Circle{Point{8, 8}, 5}, 20}
	w = Wheel{
		Circle{
			Point{
				8,
				8,
			},
			5,
		},
		20,
	}

	fmt.Printf("%#v\n", w)
	fmt.Printf("%v\n", w)

	s1 := []int{1, 2, 3, 4}
	fmt.Printf("%#v\n", s1)
	fmt.Printf("%v\n", s1)
}
