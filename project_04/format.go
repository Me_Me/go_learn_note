package main

import "fmt"

func main() {
	fmt.Printf("#%-5d %9.9s %.55s\n", 1, "wangpeng", "hahahah")
	fmt.Printf("#%5d %9.9s %.55s\n", 1, "wangpeng", "hahahah")
	fmt.Printf("#%+5d %9.9s %.55s\n", 1, "wangpeng", "hahahah")
	fmt.Printf("#%-4d %9.9s %.55s\n", 1, "wangpeng", "hahahah")

	fmt.Println("----------------------------------")

	fmt.Printf("#%-5d %9.9s %.55s\n", 1, "wangpeng", "hahahah")
	fmt.Printf("#%-5d %5.9s %.55s\n", 1, "wangpeng", "hahahah")
	fmt.Printf("#%-5d %3.9s %.55s\n", 1, "wangpeng", "hahahah")
	fmt.Printf("#%-5d %0.9s %.3s\n", 1, "wangpeng", "hahahah")
}
