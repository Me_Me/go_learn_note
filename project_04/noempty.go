package main

import "fmt"

func main() {
	fmt.Println(nonempty([]string{"a", "", "b"}))

}

func nonempty(strings []string) []string {
	i := 0
	for _, s := range strings {
		if s != "" {
			strings[i] = s
			i++
		}
	}
	return strings
}
