package main

import "fmt"

func main() {
	/*arr := [...]int{1, 2, 3, 4, 5}
	pa := &arr
	s1 := arr[0:3]
	s2 := arr[1:4]
	s1[1] = 33
	arr[1] = 44
	pa[2] = 55
	sn := arr[0:5]
	cap1 := cap(sn)
	fmt.Println(arr)
	fmt.Println(s1)
	fmt.Println(s2)
	fmt.Println(pa)
	fmt.Println(sn)
	fmt.Println(cap1)

	fmt.Println("---------------------------------")
	s := "abcdefghijklmn"
	str1 := s[:3]
	fmt.Println(str1)
	fmt.Println("---------------------------------")*/

	var n1 []int
	var n2 *int
	fmt.Printf("%p\n", n1)
	fmt.Printf("%p\n", n2)
	fmt.Printf("%p\n", &n1)
	fmt.Printf("%p\n", &n2)
	fmt.Println("------------------")
	s11 := "abc"
	s22 := "abc"
	sp1 := &s11
	sp2 := &s22
	fmt.Printf("%p\n", sp1)
	fmt.Printf("%p\n", sp2)

	fmt.Println("------------------")

	arr := [...]int{1, 2, 3, 4, 5}

	s1 := arr[:]
	//arr[0] = 111
	//s1 = append(s1, 333)
	//arr[2] = 666
	//s1 = append(s1, 444)
	//arr[1] = 222
	//s1 = append(s1, 555)
	//fmt.Println(arr)
	//fmt.Println(s1)
	fmt.Println("------------------")

	l := copy(s1, arr[4:])
	fmt.Println(l, s1)

}
