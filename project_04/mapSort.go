package main

import (
	"fmt"
)

func main() {
	//ages := map[string]int{
	//	"karl":  1,
	//	"jerry": 2,
	//	"tom":   3,
	//	"kim":   4,
	//}
	var ages = make(map[string]int)
	ages["karl"] = 1
	ages["jerry"] = 2
	ages["tom"] = 3
	ages["kim"] = 4
	for k, v := range ages {
		fmt.Println(k, "=>", v)
	}

	//fmt.Println(ages)
	//fmt.Println(len(ages))
	/*keys := make([]string, 0, len(ages))
	for k, _ := range ages {
		keys = append(keys, k)
	}*/
	//sort.Strings(keys)
	//for _, v := range keys {
	//	println(ages[v])
	//}

	//fmt.Println(keys)
}
