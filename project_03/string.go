package main

import "fmt"

func main() {
	//str := "hello, world"
	//
	//fmt.Println(str[:5])
	//fmt.Println(str[1:5])
	//fmt.Println(str[5:])
	//fmt.Println(str[:])
	//fmt.Println(str[0])
	////str1 := str[7:]
	////fmt.Println(str1[-1])
	//fmt.Println("\a")

	s := "hello, 世界"
	//fmt.Println(len(s))
	//fmt.Println(utf8.RuneCountInString(s))

	//for i := 0; i < len(s); {
	//	r, size := utf8.DecodeRuneInString(s[i:])
	//	fmt.Printf("%d\t%c\n", i, r)
	//	i += size
	//}
	for i, r := range s {
		fmt.Printf("%d\t%q\t%d", i, r, r)
	}
}
