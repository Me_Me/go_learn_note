package main

import (
	"fmt"
	"strings"
)

func main() {
	s := "d:/b/cccc.go"
	fmt.Println(basename(s))
}

/*func basename(s string) string {
	for i := len(s) - 1; i >= 0; i-- {
		if string(s[i]) == "/" {
			s = s[i+1:]
			break

		}
	}
	for i := len(s) - 1; i >= 0; i-- {
		if string(s[i]) == "." {
			s = s[:i]
			break
		}
	}
	return s
}*/

func basename(s string) string {
	i := strings.LastIndex(s, "/")
	s = s[i+1:]
	i = strings.LastIndex(s, ".")
	if i > -1 {
		s = s[:i]

	}
	return s

}
