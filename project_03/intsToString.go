package main

import (
	"bytes"
	"fmt"
)

func main() {
	fmt.Printf(intsToString([]int{1, 3, 3, 4}))
}

func intsToString(value []int) string {
	var buf bytes.Buffer
	buf.WriteByte('[')
	for i, v := range value {
		if i > 0 {
			buf.WriteString(", ")
		}
		fmt.Fprintf(&buf, "%d", v)
	}
	buf.WriteByte(']')
	return buf.String()
}
