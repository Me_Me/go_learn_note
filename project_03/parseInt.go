package main

import (
	"fmt"
	"strconv"
)

func main() {
	s, err := strconv.ParseInt("128", 0, 8)

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(s)
}
