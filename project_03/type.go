package main

import (
	"fmt"
	"math"
)

func main() {
	//var ui8 uint8 = 0
	//var i8 int8 = -128
	//fmt.Println(ui8, ui8-1, ui8-2, ui8-3)
	//fmt.Println(i8, i8+1, i8+127)
	//fmt.Println(i8, i8+1, i8+127)
	ascii := 'a'
	unicode := '过'
	fmt.Printf("%d %[1]c %[1]q\n", ascii)
	fmt.Printf("%d %[1]c %[1]q\n", unicode)

	for x := 0; x < 8; x++ {
		fmt.Printf("x = %d e³ = %8.3f\n", x, math.Exp(float64(x)))
	}
	fmt.Println(math.NaN())

}
