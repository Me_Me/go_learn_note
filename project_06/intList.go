package main

import "fmt"

type IntList struct {
	Value int
	Tail  *IntList
}

func (list *IntList) sum() int {
	if nil == list {
		return 0
	}
	return list.Value + list.Tail.sum()
}

func main() {
	l1 := IntList{1, nil}
	l2 := IntList{2, &l1}
	l3 := IntList{3, &l2}
	l4 := IntList{4, &l3}

	fmt.Println(l4.sum())

	fmt.Printf("as")
}
