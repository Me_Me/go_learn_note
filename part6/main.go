// 方法值调用

package main

import "fmt"

type Student struct {
	id   int
	name string
}

// 指针方法

func (s *Student) PointerMethod() {
	fmt.Printf("我是指针方法:%p,%v\n", s, s)
}

// 值方法

func (s Student) ValueMethod() {
	fmt.Printf("我是值方法:%p,%v\n", &s, s)
}

func main() {
	// 方法表达式
	stu := Student{1, "小明"}
	// 这里编译器不会帮你转化，所以方法的调用者必须正确，否则会报错
	pointerMethod := (*Student).PointerMethod
	//不能写成Student.PointerMethod
	pointerMethod(&stu) //我是指针方法:0xc000004078,&{1 小明}

	valueMethod := (*Student).ValueMethod
	valueMethod(&stu) //我是值方法:0xc0000040a8,{1 小明}

}
