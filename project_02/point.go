package main

import "fmt"

func main() {
	v := 1
	p1 := incr(&v)
	p2 := incr(&v)
	fmt.Println(p1, p2)
	fmt.Println(&p1, &p2)

	var q *int
	fmt.Println(q == nil)
}

func incr(p *int) int {
	*p++
	return *p
}
