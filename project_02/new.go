package main

import "fmt"

func main() {
	p := new(int)
	fmt.Println(*p)
	s := struct {
	}{}
	a := [0]int{}
	fmt.Println(s, a, &s, &a)
	fmt.Printf("%p|%p", &s, &a)
}
