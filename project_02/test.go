package main

import "fmt"

func main() {
	var x, y int
	a := &x
	arr := [5]int{1, 2, 3, 4, 5}
	fmt.Println(&x, *a, &x == nil, &x == &x, &x == &y, &arr[0], &arr[1], &arr[2], &arr[3], &arr[4])
}
