package main

import "fmt"

func main() {
	//fmt.Println(8 % 16)
	//res := gcd(6, 9)
	//fmt.Println(res)
	fmt.Println(fib(0))
	fmt.Println(fib(1))
	fmt.Println(fib(2))
	fmt.Println(fib(3))
	fmt.Println(fib(4))
	fmt.Println(fib(5))
	fmt.Println(fib(6))
	fmt.Println(fib(7))
	fmt.Println(fib(8))

}
func gcd(x, y int) int {
	for y != 0 {
		x, y = y, x%y
		//x=16,y=8
		//x=8,y=0
	}
	return x
}

func fib(n int) int {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		x, y = y, x+y
	}
	return x
}
