package main

import (
	"fmt"
	"log"
	"os"
)

var cmd string

func init() {
	var err error
	cmd, err = os.Getwd()
	if err != nil {
		log.Fatalf("os.Getwd faild : %v", err)
	}
}

func main() {
	fmt.Println(cmd)
}
