package main

import "fmt"

type IntSet struct {
	a int
}

func (*IntSet) String() string {
	return "hello"
}

func main() {
	fmt.Println(IntSet{a: 1}.String()) //编译错误 cannot call pointer method on IntSet{...}

	i1 := IntSet{a: 1}
	fmt.Println(i1.String())
}
