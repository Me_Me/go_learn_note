package main

import (
	"bytes"
	"fmt"
)

func main() {
	var buf *bytes.Buffer

	fmt.Println(buf)
	fmt.Printf("%p\n", buf)
	fmt.Println(buf == nil) //true
	//buf.Write([]byte("done!\n"))
	buf = new(bytes.Buffer)
	fmt.Println(buf)
	fmt.Printf("%p\n", buf)
	buf.Write([]byte("done!\n"))
	fmt.Println(buf == nil) //false

}
