package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	var w io.Writer
	w = os.Stdout
	w.Write([]byte("hello"))
	fmt.Println()
	w = new(bytes.Buffer)
	w.Write([]byte("hello buffer"))
	fmt.Println(w)
}
