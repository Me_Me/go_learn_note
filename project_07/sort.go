package main

import (
	"fmt"
	"sort"
)

type IntSlice []int

func main() {
	//int1 := IntSlice{3, 4, 1, 2, 9, 0, 8, 2, 6}
	//sort.Sort(int1)
	//fmt.Println(int1)
	int2 := []int{3, 4, 1, 2, 9, 0, 8, 2, 6}
	sort.Ints(int2)
	fmt.Println(int2)

}
func (p IntSlice) Len() int {
	return len(p)
}

func (p IntSlice) Less(i, j int) bool {
	return p[i] > p[j]
}

func (p IntSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}
