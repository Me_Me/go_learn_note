package main

import (
	"fmt"
	"log"
)

func main() {
	var m1 map[int]string
	fmt.Printf("%p\n", m1)
	defer func() {
		if p := recover(); p != nil {
			log.Fatalf("err: %v", p)
		}
	}()
	f1()
	panic("err test")
}

func f1() {
	panic("f1 test")
}
