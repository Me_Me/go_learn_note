package main

import "fmt"

func main() {
	m1 := map[int]string{
		1: "a",
		2: "b",
	}
	f1(m1, 2)
	fmt.Println(m1)
}

func f1(m1 map[int]string, k int) {
	m1[k] = "hahaha"
}
