package main

import (
	"fmt"
	"unsafe"
)

type point struct {
	x, y int
}

type value struct {
	id   int
	name string
	data []byte
	next *value
	point
}

func main() {
	v := struct {
		a struct{}
		b int
		c struct{}
	}{}
	fmt.Printf("c %p %d %d\n", &v, unsafe.Sizeof(v), unsafe.Alignof(v))

}

func test(x map[string]int64) {
	x["b"] = 2
	fmt.Printf("x: %p\n", x) //x: 0xc000090480
}
