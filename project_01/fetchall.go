package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func main() {
	start := time.Now()
	ch := make(chan string)

	for _, url := range os.Args[1:] {
		go fetch(url, ch)
	}
	for range os.Args[1:] {
		fmt.Println(<-ch) //从通道中读取
	}
	fmt.Println("%.2fs elapsed\n", time.Since(start).Seconds())

}

//ch chan <- string ---单向通道 -只写
func fetch(url string, ch chan<- string) {
	start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		ch <- fmt.Sprint(err)
		return
	}
	//copy返回copy字符的长度
	nbytes, err := io.Copy(ioutil.Discard, resp.Body) //丢弃掉 body中的数据
	resp.Body.Close()                                 //关闭resp资源
	if err != nil {
		ch <- fmt.Sprintf("while reading %s:%v", url, err)
		return
	}
	secs := time.Since(start).Seconds()
	ch <- fmt.Sprintf("%.2fs %7d %s", secs, nbytes, url)
}
