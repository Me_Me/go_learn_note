package main

import "fmt"

const (
	read   byte = 1 << iota // 0001
	write                   //0010
	exec                    //0100
	freeze                  //1000
)

func main() {
	//fmt.Printf("%04b  %04b  %04b %04b\n", read, write, exec, freeze)
	a := read | write | freeze                   // 0001 | 0010 | 1000 = 1011
	b := read | freeze | exec                    //0001 | 1000 | 0100 = 1101
	c := a &^ b                                  // 1011 &^ 1101   =  1011 & 0010 = 0010
	fmt.Printf("%04b &^ %04b = %04b\n", a, b, c) //1011 &^ 1101 = 0010

	d := 1
	d++
	println(d)
	//正确示例
	type data struct {
		x int
		s string
	}
	var a2 = data{1, "abc"}
	b2 := data{
		1,
		"abc",
	}
	fmt.Println(a2, b2)
	switch z := 5; z {

	case 5:
		z += 10
		println(z)
		fallthrough
	case 6:
		z += 20
		println(z)
		fallthrough
	default:
		z += 100
		println(z)
	}
	//输出 15 35 135
	dt := [3]int{10, 20, 30}

	for i1, x1 := range dt {
		if 0 == i1 {
			dt[0] += 100
			dt[1] += 200
			dt[2] += 300
		}
		fmt.Printf("x1:%d,dt:%d\n", x1, dt[i1])
	}

	for i1, x1 := range dt[:] {
		if 0 == i1 {
			dt[0] += 100
			dt[1] += 200
			dt[2] += 300
		}
		fmt.Printf("x1:%d,dt:%d\n", x1, dt[i1])
	}

	for index, val := range data1() {
		println(index, val)
	}

}
func data1() []int {
	println("origin data")
	return []int{10, 20, 30}
}
