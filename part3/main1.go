package main1

import (
	"fmt"
)

func main1() {
	const v = 20 //无显式类型声明的变量
	fmt.Printf("%T,%v\n", v, v)
	var a byte = 10
	b := a + v                  //v自动转换为byte/uint8类型
	fmt.Printf("%T,%v\n", v, v) // int,20 v还是int类型，上面只是计算时隐式转换成uint8
	fmt.Printf("%T,%v\n", b, b) //uint8,30

	f := 20
	x := 1 << f
	println(x)

	//AND NOT
	n1 := 6
	n2 := 11
	n3 := n1 &^ n2         // 0101 &^ 1011  //右操作数取反后与左操作数求&
	fmt.Printf("%04b", n3) // 0100

}
