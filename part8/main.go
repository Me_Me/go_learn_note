package main

import (
	"fmt"
)

type N int

func (n N) toString() string {
	return fmt.Sprintf("%#x", n)
}

func (n N) value() { //值复制
	n++
	fmt.Printf("v: %p, %v\n", &n, n)
}

func (n *N) pointer() { //指针传递
	*n++
	fmt.Printf("p: %p, %v\n", n, *n)
}

//var wg sync.WaitGroup

func Count(ch chan int) {
	ch <- 1
	fmt.Println("Counting...")
	//wg.Done()

}

func main() {

	/*var wg sync.WaitGroup
	wg.Add(3)

	a, b := make(chan int), make(chan int)

	go func() {
		defer wg.Done()
		for {

			select {
			case x, ok := <-a:

				if !ok {
					a = nil
					break
				}

				println("a", x) //输出接收的数据信息
			case x, ok := <-b:
				if !ok {
					b = nil
					break
				}
				println("b", x)
			}

			if nil == a && nil == b {
				return
			}

		}

	}()

	go func() {
		defer wg.Done()
		defer close(a)

		for i := 0; i < 3; i++ {
			a <- i

		}
	}()
	go func() {
		defer wg.Done()
		defer close(b)

		for i := 0; i < 5; i++ {
			b <- i * 10

		}
	}()
	wg.Wait()*/
	/*
	   a 0
	   	a 1
	   	a 2
	   	b 0
	   	b 10
	   	b 20
	   	b 30
	   	b 40
	*/

	/*c := make(chan int, 2)
	var recv <-chan int = c

	close(recv) //invalid operation: close(recv) (cannot close receive-only channel)*/
	/*c := make(chan int, 2)

	var send chan<- int = c
	var recv <-chan int = c
	<-send    //invalid operation: <-send (receive from send-only type chan<- int)
	recv <- 1 //invalid operation: recv <- 1 (send to receive-only type <-chan int)*/
	/*var wg sync.WaitGroup
	wg.Add(2)

	c := make(chan int)
	var send chan<- int = c
	var recv <-chan int = c

	go func() {
		defer wg.Done()

		for x := range recv {
			println(x)
		}
	}()

	go func() {
		defer wg.Done()
		defer close(c)

		for i := 0; i < 3; i++ {
			send <- i
		}
	}()

	wg.Wait()*/

	/*chs := make([]chan int, 10)

	//fmt.Printf("type:%T value:%v\n", chs, chs)
	for i := 0; i < 10; i++ {
		//wg.Add(1)
		chs[i] = make(chan int)
		//println(cap(chs[i]))
		//os.Exit(0)
		//println("make chs[", i, "]")
		go Count(chs[i])
	}
	for _, ch := range chs {
		//println(1)
		<-ch
		//println(a)
	}*/
	//wg.Wait()
	//os.Exit(0)
	//var a N = 1
	//a.value()                        //v: 0xc0000180c0, 2
	//fmt.Printf("a: %p, %v\n", &a, a) //a: 0xc0000180a8, 1
	//a.pointer()                      //p: 0xc0000180a8, 2
	//fmt.Printf("a: %p, %v\n", &a, a) //a: 0xc0000180a8, 2
}
