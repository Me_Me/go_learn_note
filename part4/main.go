package main

import (
	"errors"
	"log"
)

func catch() {
	log.Println("catch:", recover())
}
func main() {
	//defer catch()
	//defer log.Fatalln(recover())
	defer recover()
	panic("i am dead")
}

var errDivZero = errors.New("division by zero")

func div(x, y int) (int, error) {
	if 0 == y {

		return 0, errDivZero
	}
	return x / y, nil
}

func a() {

}

func b() {

}
